#include<stdio.h> 
#include<SQLAPI.h>         // main SQLAPI++ header 

int main(int argc, char* argv[])
{
	// create connection object to connect to database 
	SQLBase con;
	try
	{
		// connect to database 
		// in this example, it is Oracle, 
		// but can also be Sybase, Informix, DB2 
		// SQLServer, InterBase, SQLBase and ODBC 
		con.Connect("WIN-SRM7160NCOB",    // database name 
			"",  // user name 
			"",  // password 
			SQLBase); //Oracle Client 
		printf("We are connected!\n");

		// Disconnect is optional 
		// autodisconnect will occur in destructor if needed 
		//con.Disconnect();
		//printf("We are disconnected!\n");
	}


	system("PAUSE");
	return 0;
}
