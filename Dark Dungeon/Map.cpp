//Game Main
//David Deschenes
#include <iostream>
#include <string>
#include <ctime>
using namespace std;

int main()
{	
	//***CHARACTER CREATION***
	int health, level, str, dex, intel, playerxp = 0; //Character stats health, level, strength, dexterity, intelligence and total experience
	string armor, weapon, playerName, className;//Character armor, weapon
	char classType; //classtype (Warrior, Rogue and Mage)
	int ac, toHit; //Character stats
	bool onstairs = false;
	cout << "Welcome to Dark Dungeon" << endl;
	cout << "Who are you? ";
	cin >> playerName;
	cout << "Choose a class from the list below (Type the begginning letter for the desired class ie. Warrior = w)" << endl;
	cout << "Warrior" << endl;
	cout << "Assassin" << endl;
	cout << "Mage" << endl;
	cout << "Please select a class: ";
	cin >> classType;
	switch (classType) {
	case 'w':
		//Warrior starting stats and equipment
		className = "Warrior";
		health = 12;
		level = 1;
		str = 4;
		dex = 2;
		intel = 1;
		armor = "Plate";
		weapon = "Iron Sword";//Iron sword has random damage 1-8
		ac = 14; //Default ac for plate = 14
		toHit = 2; //toHit is your dex and will be added to a random roll vs enemy
		break;
	case 'a':
		//Assassin starting stats and equipment
		className = "Assassin";
		health = 12;
		level = 1;
		str = 2;
		dex = 4;
		intel = 1;
		armor = "Leather"; //Leather armor has a base of 8 then gets additional ac based on Dex
		weapon = "Iron Axe"; //Iron Axe has random roll 1-8
		ac = 12;//Leather armor has a base of 8 then gets additional ac based on Dex
		toHit = 4;//toHit is your dex and will be added to a random roll vs enemy
		break;
	case 'm':
		//Mage starting stats and equipment
		//Mage is an option now that will be better when the game is more developed as right now there are no spells added***
		className = "Mage (There are currently no spells in the game so using this class will make things extremely difficult)";
		health = 12;
		level = 1;
		str = 1;
		dex = 2;
		intel = 4;
		armor = "Leather";
		weapon = "Iron Sword";
		ac = 10;
		toHit = 2;
		break;
	default:
		cout << "Invalid choice please try again: " << endl;
		break;
	}
	cout << "Welcome " << playerName << " the " << className << endl;
	cout << "Your goal is to reach the treasure at the bottom of this dungeon" << endl << "To descend you will be looking for stairs on the map they appear as a > symbol" << endl;
	cout << "You will see a variety of enemies on your journey who will attack on site they are marked as G and O" << endl;
	//Use when testing numbers 
	//cout << str << endl << dex << endl << intel << endl;

	//***MAP***
	srand(time(0));
	char lvl1[20][20];
	int x, y, count;
	for (x = 0; x < 20; x++)
		for (y = 0; y < 20; y++)
			lvl1[x][y] = '.';

	for (count = 0; count < 7; count++)
	{
		do
		{
			x = (rand() % 20); // random row
			y = (rand() % 20); // random column
		} while (lvl1[x][y] != '.'); // So we don't over-write a used space
		lvl1[x][y] = 'G';// place Goblin at lvl1[x][y]
	}
	for (count = 0; count < 3; count++)
	{
		do
		{
			x = (rand() % 20); // random row
			y = (rand() % 20); // random column
		} while (lvl1[x][y] != '.'); // So we don't over-write a used space
		lvl1[x][y] = 'O';// place Orc at lvl1[x][y]
	}
	for (count = 0; count < 100; count++)
	{
		do
		{
			x = (rand() % 20); // random row
			y = (rand() % 20); // random column
		} while (lvl1[x][y] != '.'); // So we don't over-write a used space
		lvl1[x][y] = '|';// place wall at lvl1[x][y]
	}
	lvl1[0][0] = 'P'; // Player
	lvl1[15][15] = '>'; // Stairs

	for (x = 0; x < 20; x++)
	{
		for (y = 0; y < 20; y++)
		{
			cout << lvl1[x][y] << " ";
		}
		cout << endl;
	}


	//***MOVEMENT***
	char move;
	int px = 0, py = 0;
	cout << "In order to move the player you will need to use the WASD and QEZC keys." << endl;
	cout << "w is up, a is left, s is down and d is right" << endl << "q is up and left, e is up and right, z is down left and c is down and right" << endl;
	cout << "After each input please hit enter to move the character: ";
	cin >> move;

	do {
	system("CLS");
	switch (move) {
	case 'w':
		if (lvl1[px - 1][py] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px - 1][py] = 'P';
			px = px - 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}
		break;
	case 'a':
		if (lvl1[px][py - 1] != '|'&& lvl1[px][py - 1] > 0)
		{
			lvl1[px][py] = '.';
			lvl1[px][py - 1] = 'P';
			py = py - 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}

		break;
	case 's':
		if (lvl1[px + 1][py] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px + 1][py] = 'P';
			px = px + 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}
		break;
	case 'd':
		if (lvl1[px][py + 1] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px][py + 1] = 'P';
			py = py + 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}
		break;
	case 'q':
		if (lvl1[px - 1][py - 1] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px - 1][py - 1] = 'P';
			py = py - 1;
			px = px - 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}
		break;
	case 'e':
		if (lvl1[px - 1][py + 1] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px - 1][py + 1] = 'P';
			py = py + 1;
			px = px - 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}

		break;
	case 'z':
		if (lvl1[px + 1][py - 1] != '|')
		{
			lvl1[px][py] = '.';
			lvl1[px + 1][py - 1] = 'P';
			py = py - 1;
			px = px + 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}

		break;
	case 'c':
		if (lvl1[px + 1][py + 1] != '|' && lvl1[px + 1][py + 1] != 'G' && lvl1[px + 1][py + 1] != 'O')
		{
			lvl1[px][py] = '.';
			lvl1[px +1][py + 1] = 'P';
			py = py + 1;
			px = px + 1;
		}
		else
		{
			cout << "Invalid Move" << endl;
		}

		break;

	default:
		cout << "Invalid choice please try again: " << endl;
		break;
	}
	//***COMBAT***
	char playerAction;
	int attack, damage, run = 0;
	//Goblin Statblock
	int gobhealth = 10, gobstr = 2, gobdex=2, gobxp=10; //Goblin stats health, strength, dexterity and experience given for killing
	int gobac=12, gobToHit=2; //Goblin ac and hit modifier
	if (lvl1[px][py + 1] == 'G' || lvl1[px][py - 1] == 'G' || lvl1[px + 1][py] == 'G' || lvl1[px - 1][py] == 'G' || lvl1[px - 1][py - 1] == 'G' || lvl1[px + 1][py + 1] == 'G' || lvl1[px - 1][py + 1] == 'G' || lvl1[px + 1][py - 1] == 'G')
	{
		cout << "A goblin rabbles at you in the distance then charges" << endl;
		system("PAUSE");
		system("CLS");
		cout << "The goblin closes the distance then pulls out a short short to swing at you." << endl;
		do
		{
			int badchoice = 0;
			cout << "What would you like to do?" << endl;
			cout << "1. Attack with " << weapon << endl;
			cout << "2. Cast a spell" << endl;
			cout << "3. Run away" << endl;
			cin >> playerAction;
				switch (playerAction)
				{
				case '1':
					//Player Attack
					attack = ((int)(1 + rand() % 20) + dex);
					if (attack >= gobac)
					{
						damage = ((int)(1 + rand() % 8) + str);
						gobhealth = gobhealth - damage;
						cout << "You strike the Goblin for " << damage << " damage, leaving it with " << gobhealth << " health remaining" << endl;
					}
					else
						cout << "Your attack misses the Goblin" << endl;
					break;
				case '2':
					badchoice = 1;
					cout << "No Spells known" << endl;

					break;
				case '3':
					run = 1;
					cout << "You run away as the goblin takes an attack against you." << endl;
					break;
				default:
					badchoice = 1;
					playerAction = 0;
					cout << "Invalid choice please try again: " << endl;
					break;
				}
				cout << endl;
				//Goblin attack
				attack = ((int)(1 + rand() % 20) + gobdex);
				if (gobhealth > 0 && badchoice == 0)
				{
					if (attack >= ac)
					{
						damage = ((int)(1 + rand() % 8) + gobstr);
						health = health - damage;
						cout << "The Goblin strikes you for " << damage << " damage, leaving you with " << health << " health remaining" << endl;
					}
					else
						cout << "The Goblin narrowly misses you" << endl;
				}	
		} while (gobhealth > 0 && run == 0 && health > 0);
		system("PAUSE");
		system("CLS");
	}
	else
	{}
	//Orc statblock
	int orchealth =15, orcstr=4, orcdex=0, orcxp=30; //Orc stats health, strength, dexterity and experience given for killing
	int orcac=10, orcToHit=0; //Orc ac and hit modifier
	if (lvl1[px][py + 1] == 'O' || lvl1[px][py - 1] == 'O' || lvl1[px + 1][py] == 'O' || lvl1[px - 1][py] == 'O' || lvl1[px - 1][py - 1] == 'O' || lvl1[px + 1][py + 1] == 'O' || lvl1[px - 1][py + 1] == 'O' || lvl1[px + 1][py - 1] == 'O')
	{
		cout << "An Orc screams in your direction, as a rock whistles past your head." << endl;
		system("PAUSE");
		system("CLS");
		cout << "The orc closes the distance then pulls out a club to swing at you." << endl;
		do
		{
			int badchoice = 0;
			cout << "What would you like to do?" << endl;
			cout << "1. Attack with " << weapon << endl;
			cout << "2. Cast a spell" << endl;
			cout << "3. Run away" << endl;
			cin >> playerAction;
			switch (playerAction)
			{
			case '1':
				//Player Attack
				attack = ((int)(1 + rand() % 20) + dex);
				if (attack >= orcac)
				{
					damage = ((int)(1 + rand() % 8) + str);
					orchealth = orchealth - damage;
					cout << "You strike the Orc for " << damage << " damage, leaving it with " << orchealth << " health remaining" << endl;
				}
				else
					cout << "Your attack misses the Orc" << endl;
				break;
			case '2':
				badchoice = 1;
				cout << "No Spells known" << endl;

				break;
			case '3':
				run = 1;
				cout << "You run away as the Orc swings at your midsection." << endl;
				break;
			default:
				badchoice = 1;
				playerAction = 0;
				cout << "Invalid choice please try again: " << endl;
				break;
			}
			cout << endl;
			//Goblin attack
			attack = ((int)(1 + rand() % 20) + orcdex);
			if (orchealth > 0 && badchoice == 0)
			{
				if (attack >= ac)
				{
					damage = ((int)(1 + rand() % 8) + orcstr);
					health = health - damage;
					cout << "The Orc smacks you for " << damage << " damage, leaving you with " << health << " health remaining" << endl;
				}
				else
					cout << "The Orc narrowly misses your head as the club sails by." << endl;
			}

		} while (orchealth > 0 && run == 0 && health > 0);
		system("PAUSE");
		system("CLS");
	}
	else
	{
	}
	//***STAIRS***
	if ((px == 15) && (py == 15))//If player coordinates are equal to the stairs
	{
		onstairs = true;
		cout << "You walk down the stairs to the floor below, enter any key and press enter to continue." << endl;
	}
	else
	{}


	for (x = 0; x < 20; x++)
	{
		for (y = 0; y < 20; y++)
		{
			cout << lvl1[x][y] << " ";
		}
		cout << endl;
	}
	cin >> move;
	//Game over
	if (health <= 0)
	{
		cout << "You died" << endl;
	}
	}while (onstairs != true && health > 0);
	
	system("PAUSE");
	return 0;
}